
# https://stackoverflow.com/questions/23853632/which-kind-of-interpolation-best-for-resizing-image
# https://medium.com/analytics-vidhya/opencv-findcontours-detailed-guide-692ee19eeb18
# https://docs.opencv.org/3.4/d4/d73/tutorial_py_contours_begin.html
# https://docs.opencv.org/3.4/d3/dc0/group__imgproc__shape.html#ga17ed9f5d79ae97bd4c7cf18403e1689a
# https://stackoverflow.com/questions/19222343/filling-contours-with-opencv-python
# https://stackoverflow.com/questions/27035672/cv-extract-differences-between-two-images
# 

"""
    Cubic interpolation is computationally more complex, and hence slower than linear interpolation. 
    However, the quality of the resulting image will be higher.
"""
import os
import shutil

import cv2
import numpy as np

import libs
from tools import list_files, load_from_json
from image_tools import show_image
from image_manipulation import color_Range_Mask, erosion, dilation


PATH_ROOT = "./dataset_combined"
# PATH_IMAGE = "/images/"
PATH_IMAGE = "/images_processed_resize/"
PATH_IMAGE_SAVE = PATH_ROOT + "/images_processed_color/"
# corp_hsv_1, corp_hsv_2, corp_rgb_1 
PATH_COLOR_CONFIG_LEAF = PATH_ROOT + "./configs_color/corp_rgb_1.json"
PATH_COLOR_CONFIG_TOMATO = PATH_ROOT + "./configs_color/tomato_rgb_2.json"
COLOR_THRESHOLD = 15


def process_color_filter(path_read, path_write, config_path_color, extract_config_path_color, color_threshold, clean_start=False, verbose=True):
    if verbose:
        print("=================")
        print("Color Filter Process Initializing...")
        print("=================")

    if not os.path.exists(path_write):
        print(
            f"Creating save directory '{path_write}'...") if verbose else None
        os.makedirs(path_write, exist_ok=True, mode=0o777)
        os.makedirs(path_write + "eliminated/", exist_ok=True, mode=0o777)
    elif clean_start:
        print(f"Cleaning save directory '{path_write}'...") if verbose else None
        shutil.rmtree(path_write, ignore_errors=False, onerror=None)
        os.makedirs(path_write, exist_ok=True, mode=0o777)
        os.makedirs(path_write + "eliminated/", exist_ok=True, mode=0o777)

    # TODO
    list_file = list_files(path_read, extensions=["png", "jpg", "jpeg"])
    if verbose:
        print("Dataset Size:", len(list_file))

    config = load_from_json(config_path_color)
    config_extract = load_from_json(extract_config_path_color)

    for path_image in list_file:
        # print("File:", file)
        name_image = path_image.split("/")[-1]
        image = cv2.imread(path_image)
        threshold_k = image.shape[0] * image.shape[1] * color_threshold / 100
        print(
            f"Calculated %{color_threshold} Threshold for {image.shape[:-1]}={image.shape[0] * image.shape[1]} is {threshold_k}"
        ) if verbose else None

        if verbose:
            print(f"Processing {name_image}")
            print("Starting Color Filtering...")
            
        mask, max_matched_frame_coords = color_Range_Mask(
            img=image,
            color_palette_lower=(
                config["Lower_Blue"],
                config["Lower_Green"],
                config["Lower_Red"]
            ),
            color_palette_upper=(
                config["Upper_Blue"],
                config["Upper_Green"],
                config["Upper_Red"]
            ),
            is_HSV=config["is_HSV"],
            get_Max=False
        )
        mask_extract, _ = color_Range_Mask(
            img=image,
            color_palette_lower=(
                config_extract["Lower_Blue"],
                config_extract["Lower_Green"],
                config_extract["Lower_Red"]
            ),
            color_palette_upper=(
                config_extract["Upper_Blue"],
                config_extract["Upper_Green"],
                config_extract["Upper_Red"]
            ),
            is_HSV=config_extract["is_HSV"],
            get_Max=False
        )
        # mask_diff = mask * mask_extract
        # mask_diff[mask_diff == 1] = 255
        
        mask_common = mask * mask_extract
        mask_common[mask_common != 0] = 255
        mask_after = mask - mask_common
        mask = mask_after

        # image_temp = image.copy()
        # img_after = cv2.bitwise_and(image, image_temp, mask=mask_after)
        # img_mask = cv2.bitwise_and(image, image_temp, mask=mask)
        # image_extract = cv2.bitwise_and(image, image_temp, mask=mask_extract)
        
        # show_image(
        #     source=[
        #         image, img_after, img_mask, image_extract,
        #         mask, mask_extract, mask_common, mask_after,
        #     ],
        #     title=[
        #         "image", "img_after", "img_mask", "image_extract",
        #         "mask", "mask_extract", "mask_common", "mask_after",
        #     ],
        #     open_order=4
        # )

        # if not np.array_equal(mask, mask_after):
        #     show_image(
        #         source=[
        #             image, mask_after,
        #             mask, mask_extract, 
        #             mask_common
        #         ],
        #         title=[
        #             "image", "mask_after",
        #             "mask", "mask_extract",
        #             "mask_common"
        #         ],
        #         open_order=2
        #     )
            
        mask = 255 - mask if config["is_Invert"] else mask

        if verbose:
            print("Applying Morphological Operations to Color Mask...")

        if config["Color_Mask_Kernel"]:
            kernel = np.ones((
                config["Color_Mask_Kernel_Min"],
                config["Color_Mask_Kernel_Max"],
            ), np.uint8)

            for _ in range(config["Erosion"]):
                mask = erosion(mask, kernel)
            for _ in range(config["Dilation"]):
                mask = dilation(mask, kernel)

        if threshold_k >= 10:
            contours, hierarchy = cv2.findContours(
                mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
            )
            mask_threshold = mask.copy()
            contour_areas = list()
            for contour in contours:
                contour_areas.append(cv2.contourArea(contour))
                
                if cv2.contourArea(contour) < threshold_k:
                    cv2.fillPoly(mask_threshold, pts=[contour], color=(0, 0, 0))
                    # cv2.fillPoly(image, pts=[contour], color=(0, 0, 0))
                else:
                    cv2.fillPoly(mask_threshold, pts=[contour], color=(255, 255, 255))
                    # cv2.fillPoly(image, pts=[contour], color=(255, 255, 255))

            if not np.any(mask_threshold):
                cv2.imwrite(path_write + "eliminated/" + name_image, image)
                # # DEBUG
                # print(f"Counter Areas ({threshold_k}): {contour_areas}")
                # show_image(
                #     source=[image, mask, mask_threshold],
                #     title=["image", "mask", "mask_threshold"],
                #     open_order=2
                # )
                continue

        cv2.imwrite("./dataset_combined/images_only_tomato/" +
                    name_image, image)
        image[mask != 255] = (0, 0, 0)

        image = 255 - image if config["is_Invert"] else image

        cv2.imwrite(path_write + "/" + name_image, image)

    if verbose:
        print(f"Resizing finished for {len(list_file)} image(s).")

        print("=================")
        print("Process Finished.")
        print("=================")

if __name__ == '__main__':
    process_color_filter(
        path_read=PATH_ROOT+PATH_IMAGE,
        path_write=PATH_IMAGE_SAVE,
        config_path_color=PATH_COLOR_CONFIG_TOMATO,
        extract_config_path_color=PATH_COLOR_CONFIG_LEAF,
        color_threshold=COLOR_THRESHOLD,
        clean_start=True,
        verbose=False
    )
