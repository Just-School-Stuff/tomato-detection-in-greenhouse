
# https://stackoverflow.com/questions/23853632/which-kind-of-interpolation-best-for-resizing-image
"""
    Cubic interpolation is computationally more complex, and hence slower than linear interpolation. 
    However, the quality of the resulting image will be higher.
"""
import os
import shutil

import cv2

import libs
from tools import list_files

PATH_ROOT = "./dataset_combined"
PATH_IMAGE = "/images/"
PATH_IMAGE_SAVE = PATH_ROOT + "/images_processed_resize/"


def process_resize(path_read, path_write, dim=(300, 300), interpolation=cv2.INTER_AREA, clean_start=False, verbose=True):
    if verbose:
        print("=================")
        print("Resize Process Initializing...")
        print("=================")

    if not os.path.exists(path_write):
        print(
            f"Creating save directory '{path_write}'...") if verbose else None
        os.makedirs(path_write, exist_ok=True, mode=0o777)
    elif clean_start:
        print(
            f"Cleaning save directory '{path_write}'...") if verbose else None
        shutil.rmtree(path_write, ignore_errors=False, onerror=None)
        os.makedirs(path_write, exist_ok=True, mode=0o777)

    list_file = list_files(path_read, extensions=["png", "jpg", "jpeg"])
    if verbose:
        print("Dataset Size:", len(list_file))

    dim = (300, 300)

    # scale_percent = 60  # percent of original size
    # width = int(img.shape[1] * scale_percent / 100)
    # height = int(img.shape[0] * scale_percent / 100)
    # dim = (width, height)
    
    if verbose:
        print(f"Resizing Started with {dim} size.")

    for path_image in list_file:
        # print("File:", file)
        name_image = path_image.split("/")[-1]
        image = cv2.imread(path_image)
        shape_old = image.shape

        image = cv2.resize(image, dim, interpolation=interpolation)
        shape_new = image.shape
        if verbose:
            print(f"[INF]\t{name_image} | {shape_old} => {shape_new}")

        cv2.imwrite(path_write + "/" + name_image, image)

    if verbose:
        print(f"Resizing finished for {len(list_file)} image(s).")

        print("=================")
        print("Process Finished.")
        print("=================")

if __name__ == '__main__':
    process_resize(
        path_read=PATH_ROOT + PATH_IMAGE,
        path_write=PATH_IMAGE_SAVE,
        clean_start=True,
        verbose=False
    )
